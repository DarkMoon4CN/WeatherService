﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ChuanYe.WeatherService.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {


            config.EnableCors();
            // Web API 配置和服务

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
            name: "ActionApi",
            routeTemplate: "{controller}/{action}/{id}",
            defaults: new { controller = "WeatherController", action = "Get", id = RouteParameter.Optional }
        );
        }
    }
}
