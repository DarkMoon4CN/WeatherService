﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChuanYe.WeatherService.Web.Models.Weather
{
    public class DataSearchReq
    {

        /// <summary>
        ///  省
        /// </summary>
        public string Province { get; set; }


        /// <summary>
        ///  选填 市
        /// </summary>
        public string City { get; set; }



        /// <summary>
        /// 选填 区 或 县
        /// </summary>
        public string County { get; set; }

    }
}