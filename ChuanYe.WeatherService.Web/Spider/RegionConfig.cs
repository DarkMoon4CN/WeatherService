﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChuanYe.WeatherService.Web.Spider
{
    public class RegionConfig
    {

        public static List<InitConfig> Weather()
        {
            List<InitConfig> init = new List<InitConfig>() {
                   new InitConfig() { RegionChinese="北京", RegionEnglish="beijing",Url="http://www.tianqi.com/beijing/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="朝阳", RegionEnglish="chaoyang",Url="http://www.tianqi.com/chaoyang/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="海淀", RegionEnglish="haidian",Url="http://www.tianqi.com/haidian/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="顺义", RegionEnglish="shunyi",Url="http://www.tianqi.com/shunyi/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="怀柔", RegionEnglish="huairou",Url="http://www.tianqi.com/huairou/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="通州", RegionEnglish="tongzhou",Url="http://www.tianqi.com/tongzhou/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="昌平", RegionEnglish="changping",Url="http://www.tianqi.com/changping/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="丰台", RegionEnglish="fengtai",Url="http://www.tianqi.com/fengtai/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="石景山", RegionEnglish="shijingshan",Url="http://www.tianqi.com/shijingshan/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="大兴", RegionEnglish="daxing",Url="http://www.tianqi.com/daxing/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="房山", RegionEnglish="fangshan",Url="http://www.tianqi.com/fangshan/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="门头沟", RegionEnglish="mentougou",Url="http://www.tianqi.com/mentougou/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="平谷", RegionEnglish="pinggu",Url="http://www.tianqi.com/pinggu/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="东城区", RegionEnglish="dongchengqu",Url="http://www.tianqi.com/dongchengqu/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="密云", RegionEnglish="miyun",Url="http://www.tianqi.com/miyun/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="西城区", RegionEnglish="xichengqu",Url="http://www.tianqi.com/xichengqu/", Interval=3600,SpiderType=1},
                   new InitConfig() { RegionChinese="延庆", RegionEnglish="yanqing",Url="http://www.tianqi.com/yanqing/", Interval=3600,SpiderType=1},
            };
            return init;
        }

        public static dynamic FireGrade()
        {
            List<InitConfig> init = new List<InitConfig>() {
                   new InitConfig() { RegionChinese="北京", RegionEnglish="beijing",Url="http://www.bjyl.gov.cn/", Interval=3600*12,SpiderType=2},
            };
            return init;
        }

        public static dynamic AQI()
        {
            List<InitConfig> init = new List<InitConfig>() {
                   new InitConfig() { RegionChinese="北京", RegionEnglish="beijing",Url="http://www.tianqi.com/air/beijing.html", Interval=3600*12,SpiderType=3},
            };
            return init;
        }
    }

    public class InitConfig
    {

        /// <summary>
        /// 区域名称 中文
        /// </summary>
        public string RegionChinese { get; set; }

        /// <summary>
        /// 区域英文
        /// </summary>
        public string RegionEnglish { get; set; }

        /// <summary>
        /// 维护周期时间 单位秒
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// 目标地址
        /// </summary>
        public string Url { get; set; }


        /// <summary>
        /// 类型 1 天气 2 防火等级 3 空气质量指数
        /// </summary>
        public int SpiderType { get; set; }

    }
}