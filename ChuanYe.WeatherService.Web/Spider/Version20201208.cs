﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChuanYe.Utils;

namespace ChuanYe.WeatherService.Web.Spider
{
    public class Version20201208
    {
        
        public static string GetWeather(string url) 
        {
            try
            {
               return  RequestHelper.HttpGetRequest(url);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}