using System;
using System.Collections.Generic; 
using System.Linq;  
using System.Text;  

namespace ChuanYe.WeatherService.Web.Controllers
{  
	
	 public class WeatherInfo
	 { 
        /// <summary>
         /// 湿度
         /// </summary>
		public string Raining { get; set; } 

        /// <summary>
        /// 风向 
        /// </summary>
		public string Winddirection { get; set; } 

         /// <summary>
         /// 风力
         /// </summary>
		public string Windpower { get; set; } 

         /// <summary>
         /// 温度
         /// </summary>
		public string Temperature { get; set; } 

        /// <summary>
         /// 气象站类型
         /// </summary>
		public string Weathertype { get; set; } 

        /// <summary>
         /// 录入日期
         /// </summary>
		public DateTime Date{ get; set; } 
       
        /// <summary>
        /// 当前温度
        /// </summary>
        public string NowTemperature { get; set; }
    }

    public class FireGradeInfo
    {
        /// <summary>
        /// 火险等级
        /// </summary>
        public int FireGrade { get; set; }
    }

    public class AQIInfo
    {
        /// <summary>
        /// 空气质量指数
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// 空气质量指数 描述
        /// </summary>
        public string Desc { get; set; }
    }
}    

