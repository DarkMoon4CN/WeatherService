﻿using ChuanYe.WeatherService.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChuanYe.WeatherService.Web.Spider
{

    public class Adapter
    {
        public Adapter()
        {

        }

        public static string GetWeatherVersion(SpiderDataVersionEnum version, string url)
        {
            switch (version)
            {
                case SpiderDataVersionEnum.Version20201208:
                    return Version20201208.GetWeather(url);
                default:
                    return null;
            }
        }
    }
}
