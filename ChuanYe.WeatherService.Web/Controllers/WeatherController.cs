﻿using ChuanYe.Utils;
using ChuanYe.WeatherService.Web.Models.Weather;
using ChuanYe.WeatherService.Web.Spider;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SQLite;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ChuanYe.WeatherService.Web.Controllers
{

  
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public  class WeatherController : ApiController
    {
        public static NameValueCollection WeatherAreas = ConfigurationManager.AppSettings;
   
        /// <summary>
        /// 获取气象相关信息
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>1.天气 2.防火等级 3.空气质量指数</returns>
        [HttpPost]
        public ResultDto<dynamic> Get(DataSearchReq obj)
        {
            ResultDto<dynamic> result = new ResultDto<dynamic>();
            try
            {
                string url = "https://wis.qq.com/weather/common?source=pc&weather_type=observe|forecast_1h|forecast_24h|tips|rise|air";
                if (!string.IsNullOrEmpty(obj.Province))
                {
                    url += "&province=" + obj.Province;
                }

                if (!string.IsNullOrEmpty(obj.City))
                {
                    url += "&city=" + obj.City;
                }

                if (!string.IsNullOrEmpty(obj.County))
                {
                    url += "&county=" + obj.County;
                }
                var data = Adapter.GetWeatherVersion(SpiderDataVersionEnum.Version20201208, url);
                if (data != null)
                {
                    var existSql = "SELECT RecordID FROM record WHERE Province='" + obj.Province + "' AND City='" + obj.City + "' AND County='" + obj.County + "'";
                    var exist = Convert.ToInt32(SqliteHelper.GetSingle(existSql));
                    if (exist != 0)
                    {
                        var updateSql = " UPDATE record SET RecordValue=@RecordValue,RecordTime=@RecordTime,Province=@Province,City=@City,County=@County  WHERE  RecordID=@RecordID";
                        SQLiteParameter[] sp = new SQLiteParameter[]
                        {
                            new SQLiteParameter("@RecordID",exist),
                            new SQLiteParameter("@RecordValue",data),
                            new SQLiteParameter("@RecordTime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")),
                            new SQLiteParameter("@Province",obj.Province),
                            new SQLiteParameter("@City",obj.City),
                            new SQLiteParameter("@County",obj.County)
                        };
                        SqliteHelper.ExecuteSql(updateSql,sp);
                    }
                    else
                    {
                        var maxID = SqliteHelper.GetMaxID("RecordID", "record");
                        var insertSql = "INSERT INTO record(RecordID,RecordValue,RecordTime,Province,City,County) VALUES(@RecordID,@RecordValue,@RecordTime,@Province,@City,@County)";
                        SQLiteParameter[] sp = new SQLiteParameter[]
                        {
                            new SQLiteParameter("@RecordID",maxID),
                            new SQLiteParameter("@RecordValue",data),
                            new SQLiteParameter("@RecordTime",DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")),
                            new SQLiteParameter("@Province",obj.Province),
                            new SQLiteParameter("@City",obj.City),
                            new SQLiteParameter("@County",obj.County)
                        };
                        SqliteHelper.ExecuteSql(insertSql,sp);
                    }
                    result.FirstParam = Newtonsoft.Json.JsonConvert.DeserializeObject(data);
                }
                else 
                {
                    var sql = "SELECT * FROM record WHERE Province='" + obj.Province + "' AND City='" + obj.City + "' AND County='" + obj.County + "'";
                    var dataSet = SqliteHelper.Query(sql);
                    if (dataSet != null &&dataSet.Tables != null && dataSet.Tables.Count > 0)
                    {
                        var table = dataSet.Tables[0];
                        var recordValue = table.Rows[0]["RecordValue"] ?? string.Empty;
                        result.FirstParam =Newtonsoft.Json.JsonConvert.DeserializeObject(Convert.ToString(recordValue));
                    }
                }
                result.Status = 1;
                result.Message = "Requst Is Success";
            }
            catch (Exception ex)
            {
                result.Status = 500;
                result.Message = "/Weather/Get Server Error";
                LoggerHelper.Error("/Weather/Get Server Error :" + ex.ToString());
            }
            return result;
        }
    }
}
